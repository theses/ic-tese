# Macros

This package (`is-extras.sty`) provides the following macros to help with the thesis writing:
* `\eg` inserts the abbreviation *e.g.*, and its upper version `\Eg` that inserts *E.g.*. Both has the meaning *for example* from the latin *exampli gratia*.
* `\ie` inserts the abbreviation *i.e.*, and its upper version `\Ie` that inserts *I.e.*. Both has the meaning *in essence* or *that is* from the latin *id est*. It is used to clarify, not to introduce examples.
* `\cf` inserts the abbreviation *cf.*, and its upper version `\Cf` that inserts *Cf.*. Both has the meaning *compare* or *see* from the latin *confer*. It is used to compare the clause that precedes it with one that follows. Despite that the meaning *see* is widely used, the *Chicago Manual Style* does not approve that meaning.
* `\etal` inserts the abbreviation *et al.*, and its upper version `\Etal` that inserts *Et al.*. Both has the meaning *and others* from the latin *et alii*. It is used to compress the number of cited authors in a reference when it has three or more authors.
* `\etc` inserts the abbreviation *etc.*, and its upper version `\Etc` that inserts *Etc.*. Both has the meaning *and others* from the latin *et cetera*. It is used to interrupt the discourse and indicate that there are things that were omitted but they can be infer by the context.
* `\vs` inserts the abbreviation *vs.*, and its upper version `\Vs` that inserts *V.s.*. Both has the meaning *versus*. It is used to indicate both opposition or contradiction relations.
* `\wrt` inserts the abbreviation *w.r.t.*, and its upper version `\Wrt` that inserts *W.r.t.*. Both has the meaning *with respect to*. It is used to contextualize the clause where it is used.
* `\dof` inserts the abbreviation *d.o.f.*, and its upper version `\Dof` that inserts *D.o.f.*. Both has the meaning *degrees of freedom*.
* `\aka` inserts the abbreviation *a.k.a.*, and its upper version `\Aka` that inserts *A.k.a.*. Both has the meaning *also known as*.
